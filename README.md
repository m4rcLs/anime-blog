# Marcel's Anime Blog

This is my own blog about everything related to anime. :)
It is based on the static site generator [Jekyll](https://jekyllrb.com/) and powered by the [Minimal Mistakes](https://mmistakes.github.io/minimal-mistakes/) Jekyll theme.

The blog is built via GitLab CI and hosted by GitLab Pages.

# Start locally

**Prerequisites**:
- [Ruby](https://www.ruby-lang.org/en/)
- [Bundler](https://bundler.io/)
- [Jekyll](https://jekyllrb.com/)

To start the blog locally you should run the following command:

```sh
bundle exec jekyll serve -b /anime-blog
```

Now the blog should be running on port 4000 and can be visited.  

Making changes triggers Jekyll to render the pages again.  
So change things, wait a sec and hit F5. :)