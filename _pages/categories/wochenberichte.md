---
title: Animeberichte
layout: single
permalink: /animeberichte
taxonomy: animeberichte
---

<ul style="list-style-type:none">
    {% for post in site.posts %}
    <li>
            <div class="archive__item">
                <div class="archive__item-teaser">
                    <a href=".{{ post.url }}">
                        <img src="{{ post.preview_image }}" class="align-left">
                    </a>
                </div>
                <div class="archive__item-body">
                    <a href=".{{ post.url }}"><h2 class="archive__item-title">{{ post.title }}</h2></a>
                    <div class="archive__item-excerpt">
                        <p>{{ post.excerpt }}</p>
                    </div>
                    <p>
                        <a href=".{{ post.url }}" class="btn btn--primary">Zum Post</a>
                    </p>
                </div>
            </div>

    </li>
    <hr>
  {% endfor %}
</ul>
