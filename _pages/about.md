---
layout: single
title: Über mich
permalink: /about
---
Hallo und willkommen auf meinem Blog.
Ich freue mich, dass du da bist :)

![hi](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2F68.media.tumblr.com%2F1e5f2328e3984f6ef86be175eac54033%2Ftumblr_o3gwy1c4NZ1ultad9o1_500.gif&f=1&nofb=1){:.align-center}

Mein Name lautet Marcel, ich bin 23 Jahre jung und schaue Anime schon seit Kindheitstagen.  

Seit ungefähr 10 Jahren schaue ich regelmäßig und vielfältig Anime, meist aber nur im OmU (Original mit Untertitel).


---

<p><b>tl;dr:</b> Ich bin eine Lappertasche und möchte meinen emotionalen Schwulst nicht nur für mich behalten.  
Ob du meinen Blog magst oder nicht ist ganz deine Entscheidung :b</p>{:.notice--info}


**Anime ist für mich** mehr **ein Leidenschaftsthema** als bloß ein Hobby.   

![excited](https://media1.tenor.com/images/bddf2872f32eea960e56bd76093dd3c9/tenor.gif?itemid=6035620){:.align-right}  

Ich könnte stundenlang über Anime reden, schreiben, philosophieren, diskutieren oder dergleichen.  

Das Gleiche gilt bei mir übrigens auch für Videospiele ;)


Im September 2019 wurde es mir dann genug und ich wollte all diese Gedanken endlich niederschreiben.
So entstand dieser Blog über Anime, auf dem ich regelmäßig über aktuell gesehene Anime schreibe.

Dabei möchte ich meine Meinung nicht nur meinetwegen niederschrieben.  

Viel eher möchte ich mit diesem Blog meine Anerkennung und Faszination an einem der kreativsten und innovativsten Medien der heutigen Zeit aussprechen. (oder eher niederschreiben :b)  

![beauty-of-anime](https://i.pinimg.com/originals/91/8a/1a/918a1a6ff663f7f8d4e1b2e966ce1b8e.gif){:.align-center}

**Meine Blogeinträge sollen keine objektiven und kritischen Rezensionen sein**, obwohl sie in manchen Augen vielleicht so wirken.
Ich schreibe vollkommen subjektiv und emotional über Anime und was mich an gewissen Serien und Filmen bewegt.
Es muss nicht jeder mit meiner Meinung übereinstimmen oder meinen Schreibstil mögen.

Doch wenn jemand diesen Blog durch Zufall entdeckt, auf meine Empfehlung hin einen Anime schaut und dessen wahren Wert erkennt,
dann habe ich mit diesem Blog mehr erreicht, als ich mir je hätte erträumen können.
![fulfillment](https://media1.tenor.com/images/c50da0105e23eb6853de24383cc21cf8/tenor.gif?itemid=16061104){:.align-center}

So, das soll es erstmal zu mir gewesen sein.^^

## Kontakt

Ich freue mich über jede Nachricht und jeden Kommentar, sei es Kritik, Verbesserungsvorschläge oder einfach nur Lob.

Über die folgenden Wege könnt ihr mich kontaktieren:

   |
---|---
<a href="https://myanimelist.net/profile/m4rcLs" target="_blank"><img style="width: 47px; margin-left: 9px;" src="/anime-blog/images/my-anime-list.png"></a>|<a href="https://myanimelist.net/profile/m4rcLs" target="_blank">**m4rcLs**</a>
<img style="width: 64px;" src="/anime-blog/images/Discord-Logo-Color.svg">                                                                  |**m4rcLs#3423**
<a href="mailto:marcel.siegert@gmx.net"><i style="display:inline-block; margin-left:7px;" class="fas fa-envelope fa-3x"></i></a>            |<a href="mailto:marcel.siegert@gmx.net">**marcel.siegert@gmx.net**</a>


## Anime Empfehlungen

Zum Schluss möchte ich noch meine Anime-Tipps für verschiedene Genres bzw. Kategorien mit dir teilen.  

Die meisten Anime dieser Liste sind unter meinen eigenen Favoriten und ich würde mich freuen, wenn du mir deine Meinung zu diesen Anime mitteilst. :)

Interesse | 1. Empfehlung | 2. Empfehlung | 3. Empfehlung
----------|---------------|---------------|--------------
**Action** |Attack on Titan|My Hero Academia |Kimetsu no Yaiba|
**Comedy** |Chuunibyou demo Koi ga Shitai|Baka to Test to Shoukanjuu |Shokugeki no Souma
**Dramatik** |Your Name| Shigatsu Wa Kimi No Uso| Koe no Katachi
**Einsteiger** |Your Name|Prinzessin Mononoke|One Punch Man
**Einzigartigkeit** |Kill la Kill| Shinsekai Yori| Baccano!
**Psychologie** |Steins;Gate|Monster|Kokoro Connect|
**Romantik** |Toradora!|Ore Monogatari!!|Sankarea
**Story & Lore** |Berserk|Attack on Titan|Vinland Saga


