---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

permalink: /
layout: splash
header:
  image: /images/eclipse.jpg
  caption: The Eclipse by [**Mohamed Saad Art**](https://www.facebook.com/mohamedsaadart)
intro: 
  - excerpt: '# Aktuelle Posts'
---

{% include feature_row id="intro" type="center" %}

<div class="feature__wrapper">
    {% for post in site.posts limit:3 %}
        <div class="feature__item">
            <div class="archive__item">
                <div class="archive__item-teaser">
                    <a href=".{{ post.url }}"><img src="{{ post.preview_image }}"></a>
                </div>
                <div class="archive__item-body">
                    <a href=".{{ post.url }}"><h2 class="archive__item-title">{{ post.title }}</h2></a>
                    <div class="archive__item-excerpt">
                        <p>{{ post.excerpt }}</p>
                    </div>
                    <p>
                        <a href=".{{ post.url }}" class="btn btn--primary">Zum Post</a>
                    </p>
                </div>
            </div>
        </div>
  {% endfor %}
</div>